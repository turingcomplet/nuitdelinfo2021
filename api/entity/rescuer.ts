import express, { Request, Response } from 'express';

import { Rescuer } from '../models/rescuer';
import {NotFoundError} from "../common/errors/not-found-error";

const router = express.Router();

router.get('/api/entity/rescuer',
async (req: Request, res: Response) => {
  let tokenid = req.query.id;
  if(tokenid){
    const rescuer = await Rescuer.find({id: tokenid, status:"valid"});
    if (!rescuer) {
      throw new NotFoundError('Rescuer profile not found');
    }
    res.status(200).send({ rescuer });
  } else {
    const rescuers = await Rescuer.find({status:"valid"});
    res.status(200).send({ rescuers });
  }
});

export { router as rescuerRouter };
