import express, { Request, Response } from 'express';

import { Rescue } from '../models/rescue';
import {NotFoundError} from "../common/errors/not-found-error";

const router = express.Router();

router.get('/api/entity/rescue',
async (req: Request, res: Response) => {

  let tokenid = req.query.id;

  if(tokenid){
    const rescue = await Rescue.find({id: tokenid, status: "valid"});
    if (!rescue) {
      throw new NotFoundError('Rescue event not found');
    }
    res.status(200).send({ rescue });
  } else {
    const rescues = await Rescue.find({status:"valid"});
    res.status(200).send({ rescues });
  }
});

export { router as rescueRouter };
