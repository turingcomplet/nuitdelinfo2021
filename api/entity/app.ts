import { rescuerRouter } from "./rescuer"
import { rescueRouter } from "./rescue"

const setEntityRouter = (app : any) => {

    app.use(rescuerRouter),
    app.use(rescueRouter)
}

export { setEntityRouter }
