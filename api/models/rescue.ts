import mongoose from 'mongoose';


interface article {
  lang: string,
  content: string,
  source: string
}

// An interface that describes the properties
// that are required to create a new Rescuer
interface RescueAttrs {
  title: string;
  date: Date;
  rescuers_id: Array<string>;
  boats : Array<{ name: string, description: string }>;
  articles : Array<article>;
  sources: Array<string>;
};

// An interface that describes the properties
// that a User Model has
interface RescueModel extends mongoose.Model<RescueDoc> {
  build(attrs: RescueAttrs): RescueDoc;
}

// An interface that describes the properties
// that a User Document has
interface RescueDoc extends mongoose.Document {
  title: string;
  date: Date;
  rescuers_id: Array<string>;
  boats : Array<{ name: string, description: string }>;
  articles : Array<article>
  status: string;
  sources: Array<string>;
}

const rescueSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true
  },
  boats: {
    type: [{name: String, description: String }]
  },
  articles: {
    type: [{ lang: String, content: String, source: String }]
  },
  sources: {
    type: [String]
  },
  rescuers_id: {
    type: [String]
  },
  status: {
  type: String,
  required: false,
  default: "invalid"
  }
}, {
  toJSON: {
    transform(doc, ret) {
      ret.id = ret._id;
      delete ret._id;
      delete ret.password;
      delete ret.__v;
    }
  }
});

rescueSchema.statics.build = (attrs: RescueAttrs) => {
  return new Rescue(attrs);
}

const Rescue = mongoose.model<RescueDoc, RescueModel>('Rescue', rescueSchema);

export { Rescue };
