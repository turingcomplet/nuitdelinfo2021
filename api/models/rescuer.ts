import mongoose from 'mongoose';


// An interface that describes the properties
// that are required to create a new Rescuer
interface RescuerAttrs {
  name: string;
  description: string;
};

// An interface that describes the properties
// that a User Model has
interface RescuerModel extends mongoose.Model<RescuerDoc> {
  build(attrs: RescuerAttrs): RescuerDoc;
}

// An interface that describes the properties
// that a User Document has
interface RescuerDoc extends mongoose.Document {
  name: string;
  description: string;
  status: string;
}

const rescuerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  status: {
  type: String,
  required: false,
  default: "invalid"
  }
}, {
  toJSON: {
    transform(doc, ret) {
      ret.id = ret._id;
      delete ret._id;
      delete ret.password;
      delete ret.__v;
    }
  }
});

rescuerSchema.statics.build = (attrs: RescuerAttrs) => {
  return new Rescuer(attrs);
}

const Rescuer = mongoose.model<RescuerDoc, RescuerModel>('Rescuer', rescuerSchema);

export { Rescuer };