import express, { Request, Response } from 'express';

import {Rescuer} from "../models/rescuer";
// @ts-ignore
import {Rescue} from "../models/rescue";

const router = express.Router();

router.get('/api/search',
async (req: Request, res: Response) => {

  const searchString  = req.query.query as string;
  const searchArray = searchString.split(' ');

  let rescues = [];
  let rescuers = [];
  let rescuesTemp;
  let rescuersTemp;
  for(let s of searchArray){
    //rescuesTemp = await Rescue.find({articles: {content:'/'+s+'/i'},status:"valid"});
    rescuersTemp = await Rescuer.find({name: '/'+s+'/i',status:"valid"});
    rescues.push(rescuesTemp);
    rescuers.push(rescuersTemp);
  }
  res.status(200).send({ rescues: rescues, rescuers:rescuers });
});

export { router as searchRouter };
