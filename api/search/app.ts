import { searchRouter } from "./search"

const setSearchRouter = (app : any) => {
    app.use(searchRouter)
}

export { setSearchRouter }
