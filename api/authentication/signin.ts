import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import { User } from '../models/user';
import { Password } from '../services/password';

import { BadRequestError } from '../common/errors/bad-request-error';
import { validateRequest } from '../common/middlewares/validate-request';
import jwt from 'jsonwebtoken';

const router = express.Router();

router.post('/api/admin/signin', [
body('username')
  .notEmpty()
  .withMessage('please provide an username'),
body('password')
  .trim()
  .notEmpty()
  .withMessage('Password must be provided')
], validateRequest,
async (req: Request, res: Response) => {

  const {username, password } = req.body;

  const existingUser = await User.findOne({ username: username });
  if(!existingUser) {
    throw new BadRequestError('Invalid credentials');
  }

  const passwordMatch = await Password.compare(existingUser.password,password);
  if(!passwordMatch) {
    throw new BadRequestError('Invalid credentials');
  }

  // generate JWT
  const userJwt = jwt.sign(
    {
      id: existingUser.id,
      username: existingUser.username
    }, 
      process.env.JWT_KEY!
  );

  

  res.status(200).send({data: existingUser, access_token : userJwt});
});

export { router as signinRouter };