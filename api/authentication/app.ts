import { signinRouter } from "./signin"


const setAuthenticationRouter = (app : any) => {
    app.use(signinRouter);
}

export { setAuthenticationRouter }