import { app } from './app';
import mongoose from 'mongoose';
import { User } from './models/user';

const start = async () => {
    if(!process.env.JWT_KEY) {
        throw new Error('JWT_KEY must be defined');
      }
      if(!process.env.MONGO_URI) {
        throw new Error('Mongo_URI must be defined');
      }
    
      try {
        await mongoose.connect(process.env.MONGO_URI, {
        });
        console.log('connected to database')
        let existingAdmin = await User.findOne({ username: "Admin"});
        if(!existingAdmin){
          let newAdmin = User.build({username: "Admin", password: "nuitInfo2021"});
          await newAdmin.save();
        }
      }catch(err) {
        console.log(err);
        process.exit();
      }
}

app.listen(3000, () => {
    console.log('Listening on port 3000 !!!');
});


start();