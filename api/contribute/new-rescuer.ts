import express, { Request, Response } from 'express';
import { body } from 'express-validator';

import { BadRequestError } from '../common/errors/bad-request-error';
import { validateRequest } from '../common/middlewares/validate-request';
import { Rescuer } from '../models/rescuer';

const router = express.Router();

router.post('/api/contribute/new-rescuer', [
body('name')
  .notEmpty()
  .withMessage('please provide a name'),
body('description')
  .trim()
  .notEmpty()
  .withMessage('Description must be provided')
], validateRequest,
async (req: Request, res: Response) => {

  const {name, description } = req.body;

  const existingRescuer = await Rescuer.findOne({ name: name });
  if(existingRescuer) {
    throw new BadRequestError('rescuer already exist');
  }

  let newRescuer = Rescuer.build({ name: name, description: description });
  await newRescuer.save();

  res.status(201).send({ message: "Rescuer created"});
});

export { router as newRescuerRouter };