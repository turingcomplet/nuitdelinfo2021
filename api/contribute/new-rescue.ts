import express, { Request, Response } from 'express';
import { body } from 'express-validator';

import { BadRequestError } from '../common/errors/bad-request-error';
import { validateRequest } from '../common/middlewares/validate-request';
import { Rescue } from '../models/rescue';
import { Rescuer } from '../models/rescuer';

const router = express.Router();

router.post('/api/contribute/new-rescue', [
body('title')
  .notEmpty()
  .withMessage('please provide a title'),
body('date')
  .notEmpty()
  .withMessage('please provide a date'),
body('rescuers_id')
  .isArray()
  .notEmpty(),
body('sources')
  .isArray()
  .withMessage('Sources must be provided')
], validateRequest,
async (req: Request, res: Response) => {

  let {date, rescuers_id, boats, articles, sources, title } = req.body;

  const existingRescue = await Rescue.findOne({ date: date });
  if(existingRescue) {
    throw new BadRequestError('rescue already exist');
  }

let realRescuers_id : Array<string> = []
for(let rescuer of rescuers_id){
  const name = await Rescuer.findOne({ name : rescuer });
  if(name) {
    realRescuers_id.push(name._id);
  }else{
    throw new BadRequestError('Rescuers Name is not exist');
  }
}


  if(!articles){
    articles = [];
  }

  if(!boats){
    boats = [];
  }

  let newRescue = Rescue.build({
    title: title,
    date : date,
    rescuers_id : realRescuers_id,
    boats : boats,
    articles : articles,
    sources : sources
  });
  await newRescue.save();

  res.status(201).send({ message: "Rescue created"});
});

export { router as newRescueRouter };
