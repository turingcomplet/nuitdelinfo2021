import express from 'express';
import cors from 'cors';
import 'express-async-errors';
import { NotFoundError } from './common/errors/not-found-error';
import { setContributeRouter } from './contribute/app';
import { setModerationRouter } from './moderation/app';
import { setSearchRouter } from './search/app';
import { errorHandler } from './common/middlewares/error-handler';
import { setAuthenticationRouter } from './authentication/app';
import { setEntityRouter } from './entity/app';

const app = express();

let corsOption = {
    origin: '*',
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    preflightContinue: false,
    optionsSuccessStatus: 204,
    credentials: true
  }

app.use(cors(corsOption));
app.use(express.json());

setAuthenticationRouter(app);
setContributeRouter(app);
setModerationRouter(app);
setSearchRouter(app);
setEntityRouter(app);



app.all('*', () => {
    throw new NotFoundError();
});

app.use(errorHandler);

export { app };