import { indexInvalidRescueRouter } from "./index-invalid-rescue";
import { indexInvalidRescuerRouter } from "./index-invalid-rescuer";
import { invalidRescueRouter } from "./invalid-rescue";
import { invalidRescuerRouter } from "./invalid-rescuer";
import { validRescueRouter } from "./valid-rescue";
import { validRescuerRouter } from "./valid-rescuer"


const setModerationRouter = (app : any) => {
    app.use(validRescuerRouter);
    app.use(invalidRescuerRouter);
    app.use(validRescueRouter);
    app.use(invalidRescueRouter);
    app.use(indexInvalidRescueRouter);
    app.use(indexInvalidRescuerRouter);
}

export { setModerationRouter }