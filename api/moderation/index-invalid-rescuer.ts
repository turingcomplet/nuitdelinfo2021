import express, { Request, Response } from 'express';

import { BadRequestError } from '../common/errors/bad-request-error';
import { currentUser } from '../common/middlewares/current-user';
import { requireAuth } from '../common/middlewares/require-auth';
import { Rescuer } from '../models/rescuer';

const router = express.Router();

router.get('/api/moderation/invalid-rescuer', currentUser,requireAuth,
async (req: Request, res: Response) => {

  const id = req.query.id as string;
  let existingRescuer;
  if(id){
    existingRescuer = await Rescuer.findOne({ _id: id, status: "invalid" });
    if(!existingRescuer) {
      throw new BadRequestError('unexisting rescue');
    }
  }else{
    existingRescuer = await Rescuer.find({status: "invalid"});
  }


  res.status(200).send(existingRescuer);
});

export { router as indexInvalidRescuerRouter };