import express, { Request, Response } from 'express';

import { BadRequestError } from '../common/errors/bad-request-error';
import { currentUser } from '../common/middlewares/current-user';
import { requireAuth } from '../common/middlewares/require-auth';
import { Rescuer } from '../models/rescuer';

const router = express.Router();

router.post('/api/moderation/valid-rescuer', currentUser, requireAuth,
async (req: Request, res: Response) => {

  const id = req.query.id as string;

  const existingRescuer = await Rescuer.findOne({ _id: id });
  if(!existingRescuer) {
    throw new BadRequestError('unexisting rescuer');
  }

  existingRescuer.status = "valid";

  await existingRescuer.save();

  res.status(201).send({ message: "Rescuer validated"});
});

export { router as validRescuerRouter };