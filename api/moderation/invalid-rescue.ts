import express, { Request, Response } from 'express';

import { BadRequestError } from '../common/errors/bad-request-error';
import { currentUser } from '../common/middlewares/current-user';
import { requireAuth } from '../common/middlewares/require-auth';
import { Rescue } from '../models/rescue';

const router = express.Router();

router.post('/api/moderation/invalid-rescue', currentUser,requireAuth,
async (req: Request, res: Response) => {

  const id = req.query.id as string;

  const existingRescue = await Rescue.findOne({ _id: id });
  if(!existingRescue) {
    throw new BadRequestError('unexisting rescue');
  }

  await existingRescue.remove();

  res.status(201).send({ message: "Rescue invalidate"});
});

export { router as invalidRescueRouter };