import express, { Request, Response } from 'express';

import { BadRequestError } from '../common/errors/bad-request-error';
import { currentUser } from '../common/middlewares/current-user';
import { requireAuth } from '../common/middlewares/require-auth';
import { Rescue } from '../models/rescue';

const router = express.Router();

router.get('/api/moderation/invalid-rescue', currentUser,requireAuth,
async (req: Request, res: Response) => {

  const id = req.query.id as string;
  let existingRescue;
  if(id){
    existingRescue = await Rescue.findOne({ _id: id, status: "invalid" });
    if(!existingRescue) {
      throw new BadRequestError('unexisting rescue');
    }
  }else{
    existingRescue = await Rescue.find({status: "invalid"});
  }


  res.status(200).send(existingRescue);
});

export { router as indexInvalidRescueRouter };